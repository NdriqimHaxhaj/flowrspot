//
//  Flower.swift
//  FlowrSpot
//
//  Created by TK on 18/01/2018.
//  Copyright © 2018 PovioLabs. All rights reserved.
//

struct Flower: Codable {
    let id: Int?
    let name: String?
    let latinName: String?
    let sightings: Int?
    let profilePicture: String?
    let favorite: Bool?
    let features: [String]?
    let flowerDescription: String?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case latinName
        case sightings
        case profilePicture
        case favorite, features
        case flowerDescription = "description"
    }
}
