//
//  FlowerDetails.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import Foundation

// MARK: - FlowerDetails
struct FlowerDetails: Codable {
    let flower: Flower?
}
