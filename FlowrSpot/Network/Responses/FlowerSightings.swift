//
//  FlowerSightings.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import Foundation

// MARK: - FlowerSightings
struct FlowerSightings: Codable {
    let sightings: [Sighting]?
    let meta: Meta?
}

// MARK: - Meta
struct Meta: Codable {
    let pagination: Pagination?
}

// MARK: - Pagination
struct Pagination: Codable {
    let currentPage: Int?
    let prevPage, nextPage: Int?
    let totalPages: Int?
    
    enum CodingKeys: String, CodingKey {
        case currentPage = "current_page"
        case prevPage = "prev_page"
        case nextPage = "next_page"
        case totalPages = "total_pages"
    }
}

// MARK: - Sighting
struct Sighting: Codable {
    let id: Int
    let name, sightingDescription, picture: String
    let likesCount, commentsCount: Int?
    let createdAt: String?
    let latitude, longitude: Double?
    let user: User?
    let flower: Flower?
    
    enum CodingKeys: String, CodingKey {
        case id, name
        case sightingDescription = "description"
        case picture
        case likesCount
        case commentsCount
        case createdAt
        case latitude, longitude, user, flower
    }
}

// MARK: - User
struct User: Codable {
    let id: Int?
    let fullName: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case fullName
    }
}
