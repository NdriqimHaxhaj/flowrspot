//
//  AddNewSightingContentView.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 8/1/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit

class AddNewSightingContentView: UIView {
    private let testLabel = UILabel.autolayoutView()
    let rightBarButton = UIButton(type: .custom)
    let leftBarButton = UIButton(type: .custom)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension AddNewSightingContentView {
    func setupViews(){
        backgroundColor = .white
        addTestLabel()
    }
    
    func addTestLabel(){
        addSubview(testLabel)
        testLabel.font = .custom(type: .regular, size: 16)
        testLabel.text = "Add New Sighting"
        testLabel.snp.makeConstraints{
            $0.centerX.centerY.equalToSuperview()
        }
    }
}
