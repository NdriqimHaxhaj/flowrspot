//
//  AddNewSightingViewController.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 8/1/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit

class AddNewSightingViewController: UIViewController {
    
    private var contentView: AddNewSightingContentView!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nil, bundle: nil)
        contentView = AddNewSightingContentView().autolayoutView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
}

private extension AddNewSightingViewController {
    func setupViews(){
        navigationItem.title = "general_app_name".localized()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: contentView.rightBarButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: contentView.leftBarButton)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        setupContentView()
    }
    
    func setupContentView(){
        view.addSubview(contentView)
        contentView.snp.makeConstraints { $0.edges.equalToSuperview() }
        
        contentView.rightBarButton.setImage(#imageLiteral(resourceName: "plIconDots"), for: .normal)
        contentView.rightBarButton.addTarget(self, action: #selector(rightBarButtonPressed), for: .touchUpInside)
        
        contentView.leftBarButton.setImage(#imageLiteral(resourceName: "plIconBack"), for: .normal)
        contentView.leftBarButton.addTarget(self, action: #selector(leftBarButtonPressed), for: .touchUpInside)
    }
    
    @objc func rightBarButtonPressed() {
        
    }
    
    @objc func leftBarButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
}
