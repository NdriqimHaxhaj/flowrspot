//
//  FlowerSightingsDataSource.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//
import PovioKit

class FlowerSightingsDataSource: NSObject {
    private var flowerSightings: FlowerSightings?
    private var flower : Flower?
    
    func updateDetails(with flower:Flower) {
        self.flower = flower
    }
    
    func updateSightings(with flowerSightings: FlowerSightings) {
        self.flowerSightings = flowerSightings
    }
}

// MARK: - UICollectionView DataSource
extension FlowerSightingsDataSource: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sightings = flowerSightings?.sightings?.count else {return 1}
        return 1 + sightings
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0 {
            guard let flower = flower else {return UITableViewCell()}
            let cell = tableView.dequeueReusableCell(FlowerDetailsTableViewCell.self, at: indexPath)
            cell.configureCell(wiht: flower)
            cell.selectionStyle = .none
            return cell
        }
        
        guard let sighting = flowerSightings?.sightings?[indexPath.row-1] else {return UITableViewCell()}
        let cell = tableView.dequeueReusableCell(FlowerSightingTableViewCell.self, at: indexPath)
        cell.configureCell(with: sighting)
        cell.selectionStyle = .none
        return cell
    }
    
}

