//
//  FlowerDetailsInteractor.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import Foundation

protocol FlowerDetailsBusinessLogic {
    func fetchFlowerDetails(with flowerId: Int)
    func fetchFlowerSightings(with flowerId: Int)
}

class FlowerDetailsInteractor {
    var presenter: FlowerDetailsPresentationLogic?
    var getFlowerDetailsWorker = GetFlowerDetailsWorker()
}

// MARK: - Business Logic
extension FlowerDetailsInteractor: FlowerDetailsBusinessLogic {
    func fetchFlowerDetails(with flowerId: Int) {
        getFlowerDetailsWorker.details(flowerId: flowerId, flowerDataType: .details, success: { (flowerDetails) in
            guard let flower = flowerDetails.flower else {return}
            self.presenter?.presentFlowerDetails(flower)
        }) { (error) in
            self.presenter?.presentFlowersError(error)
        }
    }
    
    func fetchFlowerSightings(with flowerId: Int) {
        getFlowerDetailsWorker.sightings(flowerId: flowerId,
                                         flowerDataType: .sightings,
                                         success: { (flowerSightings) in
                                            self.presenter?.presentFlowerSightings(flowerSightings)
        }) { (error) in
            self.presenter?.presentFlowersError(error)
        }
        
    }
}

