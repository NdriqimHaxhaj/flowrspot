//
//  FlowerDetailsPresenter.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import UIKit

protocol FlowerDetailsPresentationLogic {
    func presentFlowerDetails(_ flower: Flower)
    func presentFlowerSightings(_ flowerSightings: FlowerSightings)
    func presentFlowersError(_ error: RemoteResourceError)
}

class FlowerDetailsPresenter {
    weak var viewController: FlowerDetailsDisplayLogic?
}

// MARK: - Presentation Logic
extension FlowerDetailsPresenter: FlowerDetailsPresentationLogic {
    func presentFlowerDetails(_ flower: Flower) {
        viewController?.displayFlowerDetails(flower)
    }
    
    func presentFlowerSightings(_ flowerSightings: FlowerSightings) {
        viewController?.displayFlowerSightings(flowerSightings)
    }
    
    func presentFlowersError(_ error: RemoteResourceError) {
        viewController?.displayError(error)
    }
}

