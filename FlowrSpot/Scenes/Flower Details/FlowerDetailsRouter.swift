//
//  FlowerDetailsRouter.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import UIKit

protocol FlowerDetailsRoutingLogic {
    func navigateToFlowerDetails(flower: Flower)
    func navigateToAlert(title: String, message: String, handler: (() -> Void)?)
}

protocol FlowerDetailsRouterDelegate: class {
    
}

class FlowerDetailsRouter {
    weak var viewController: FlowerDetailsViewController?
    weak var delegate: FlowerDetailsRouterDelegate?
}

// MARK: - Routing Logic
extension FlowerDetailsRouter: FlowerDetailsRoutingLogic {
    
    func navigateToFlowerDetails(flower: Flower) {
        let flowerDetailsViewController = FlowerDetailsViewController.init(delegate: nil, flower: flower)
        self.viewController?.navigationController?.pushViewController(flowerDetailsViewController, animated: true)
        
    }
    
    func navigateToAlert(title: String, message: String, handler: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "general_ok".localized(), style: .cancel, handler: { _ in handler?() }))
        viewController?.present(alert, animated: true, completion: nil)
    }
}


extension FlowerDetailsRouter: FlowerDetailsDelegate {
    func addNewSightingButtonPressed() {
        let addNewSightingViewController = AddNewSightingViewController.init()
        addNewSightingViewController.modalPresentationStyle = .none
        viewController?.navigationController?.pushViewController(addNewSightingViewController, animated: true)
    }
}
