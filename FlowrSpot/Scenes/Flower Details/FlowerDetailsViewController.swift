//
//  FlowerDetailsViewController.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit

protocol FlowerDetailsDisplayLogic: class {
    func displayFlowerSightings(_ flowerSightings: FlowerSightings)
    func displayFlowerDetails(_ flower: Flower)
    func displayError(_ error: RemoteResourceError)
}

class FlowerDetailsViewController: UIViewController {
    var interactor: FlowerDetailsBusinessLogic?
    var router: FlowerDetailsRoutingLogic?
    private var contentView : FlowerDetailsContentView!
    private let flowerSightingsDataSource = FlowerSightingsDataSource()
    private var flower:Flower?
    
    init(delegate: FlowerDetailsRouterDelegate?, flower: Flower?) {
        super.init(nibName: nil, bundle: nil)
        self.flower = flower
        let interactor = FlowerDetailsInteractor()
        let presenter = FlowerDetailsPresenter()
        let router = FlowerDetailsRouter()
        interactor.presenter = presenter
        presenter.viewController = self
        router.viewController = self
        router.delegate = delegate
        self.interactor = interactor
        self.router = router
        contentView = FlowerDetailsContentView(flower: flower).autolayoutView()
        contentView.headerView?.delegate = router
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        loadData()
    }
    
    // MARK: - Functions
    
}

// MARK: - Display Logic
extension FlowerDetailsViewController: FlowerDetailsDisplayLogic {
    func displayFlowerSightings(_ flowerSightings: FlowerSightings) {
        guard let contentView = contentView else {return}
        flowerSightingsDataSource.updateSightings(with: flowerSightings)
        contentView.tableView.reloadData()
        contentView.emptyView.isHidden = true
    }
    
    func displayFlowerDetails(_ flower: Flower) {
        self.flower = flower
        flowerSightingsDataSource.updateDetails(with: flower)
        contentView.tableView.reloadData()
        contentView.flower = flower
        contentView.headerView?.configure(with: flower)
    }
    
    func displayError(_ error: RemoteResourceError) {
        router?.navigateToAlert(title: "general_error".localized(), message: error.localizedDescription, handler: nil)
        guard let contentView = contentView else {return}
        contentView.emptyView.isHidden = false
    }
}


// MARK: - UITableView Delegate
extension FlowerDetailsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        guard let row = flowersDataSource.row(at: indexPath) else {
        //            Logger.error("No availible row in dataSource at \(indexPath)")
        //            return
        //        }
        //        switch row {
        //        case let .flower(entity):
        //            router?.navigateToFlowerDetails(flower: entity)
        //        }
    }
}

// MARK: - UIScrollView Delegate
extension FlowerDetailsViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = -scrollView.contentOffset.y
        
        let percentage = 1 - (contentOffset / contentView.headerViewHeight)
        
        var headerViewTranslation = -percentage * contentView.headerViewHeight
        if headerViewTranslation > 0 {
            headerViewTranslation = 0 // lock headerView
        }
        
        contentView.headerView?.transform = CGAffineTransform.identity.translatedBy(x: 0, y: headerViewTranslation)
    }
}

// MARK: - Private methods
private extension FlowerDetailsViewController {
    func setupViews() {
        navigationItem.title = "general_app_name".localized()
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: contentView.rightBarButton)
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: contentView.leftBarButton)
        self.navigationController?.interactivePopGestureRecognizer?.delegate = nil
        setupContentView()
    }
    
    func setupContentView() {
        view.addSubview(contentView)
        contentView.snp.makeConstraints { $0.edges.equalToSuperview() }
        
        contentView.rightBarButton.setImage(#imageLiteral(resourceName: "plIconDots"), for: .normal)
        contentView.rightBarButton.addTarget(self, action: #selector(rightBarButtonPressed), for: .touchUpInside)
        
        contentView.leftBarButton.setImage(#imageLiteral(resourceName: "plIconBack"), for: .normal)
        contentView.leftBarButton.addTarget(self, action: #selector(leftBarButtonPressed), for: .touchUpInside)
        
        contentView.tableView.delegate = self
        contentView.tableView.dataSource = flowerSightingsDataSource
    }
    
    func loadData() {
        guard let flowerId = flower?.id else {return}
        interactor?.fetchFlowerDetails(with: flowerId)
        interactor?.fetchFlowerSightings(with: flowerId)
    }
    
    @objc func rightBarButtonPressed() {
        
    }
    
    @objc func leftBarButtonPressed() {
        navigationController?.popViewController(animated: true)
    }
}


