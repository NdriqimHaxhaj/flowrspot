//
//  FlowerDetailsContentView.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit

class FlowerDetailsContentView: UIView {
    let collectionViewDimensions = FlowerCollectionViewItemDimenson(numberOfItemsInRow: 2, insets: 8)
    let headerViewHeight: CGFloat = 350
    var headerView : FlowerDetailsHeaderView?
    let tableView = UITableView(frame: .zero).autolayoutView()
    let rightBarButton = UIButton(type: .custom)
    let leftBarButton = UIButton(type: .custom)
    
    let emptyView = EmptyView.autolayoutView()
    var flower:Flower?
    
    private let featuresLabel = UILabel.autolayoutView()
    private let descriptionLabel = UILabel.autolayoutView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public convenience init(flower: Flower?) {
        self.init(frame: .zero)
        self.flower = flower
        setupViews()
    }
}

private extension FlowerDetailsContentView {
    func setupViews() {
        backgroundColor = .white
        setupTableView()
        setupHeaderView()
        setupEmptyView()
    }
    
    func setupTableView() {
        addSubview(tableView)
        tableView.backgroundColor = .white
        tableView.keyboardDismissMode = .onDrag
        tableView.estimatedRowHeight = 100
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.contentInset = UIEdgeInsets(top: headerViewHeight + 25, left: 0, bottom: 0, right: 0)
        tableView.register(FlowerDetailsTableViewCell.self)
        tableView.register(FlowerSightingTableViewCell.self)
        tableView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupHeaderView() {
        headerView = FlowerDetailsHeaderView().autolayoutView()
        headerView?.configure(with: flower)
        guard let headerView = headerView else {return}
        headerView.flower = flower
        addSubview(headerView)
        headerView.snp.makeConstraints {
            $0.leading.top.trailing.equalToSuperview()
            $0.height.equalTo(headerViewHeight)
        }
    }
    
    
    func setupEmptyView() {
        addSubview(emptyView)
        emptyView.text = "placeholder_no_content".localized()
        guard let headerView = headerView else {return}
        emptyView.snp.makeConstraints {
            $0.top.equalTo(headerView.snp.bottom)
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
}

