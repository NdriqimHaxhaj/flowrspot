//
//  FlowerDetailsHeaderView.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit
import SnapKit

protocol FlowerDetailsDelegate {
    func addNewSightingButtonPressed()
}

class FlowerDetailsHeaderView: UIView {
    private let favoriteButtonSize = CGSize(width: 30, height: 30)
    private let favoriteButton = UIButton.autolayoutView()
    private let sightingsLabel = UILabel.autolayoutView()
    private let sightingsLabelWrapperView = UIView.autolayoutView()
    private let backgroundImageView = UIImageView.autolayoutView()
    private let titleLabel = UILabel.autolayoutView()
    private let subtitleLabel = UILabel.autolayoutView()
    private let addNewSightingButton = UIButton.autolayoutView()
    private let newSightingButtonHeight:CGFloat = 50
    private let newSightingButtonWidth:CGFloat = 190
    private let searchBar = SearchView.autolayoutView()
    private var gradientLayer : CAGradientLayer!
    
    var flower: Flower?
    var delegate:FlowerDetailsDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        return searchBar.frame.contains(point)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with flower: Flower?){
        self.flower = flower
        setBackgroundImageView()
        setNameLabel()
        setLatinNameLabel()
        setSightingsLabel()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setupViews()
    }
    
}

// MARK: - Hit Test extension to receive touch events outside view bounds
extension FlowerDetailsHeaderView {
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        guard !clipsToBounds && !isHidden && alpha > 0 else { return nil }
        for member in subviews.reversed() {
            let subPoint = member.convert(point, from: self)
            guard let result = member.hitTest(subPoint, with: event) else { continue }
            return result
        }
        return nil
    }
}

// MARK: - Private methods
private extension FlowerDetailsHeaderView {
    func setupViews() {
//        layer.masksToBounds = true
//        clipsToBounds = true
        setupBackgroundImageView()
        setupFavoriteButton()
        setupSightingsLabel()
        setupLabelWrapperView()
        setupNameLabel()
        setupLatinNameLabel()
        setupAddNewSightingButton()
    }
    
    func setupFavoriteButton() {
        addSubview(favoriteButton)
        favoriteButton.backgroundColor = .white
        favoriteButton.layer.cornerRadius = favoriteButtonSize.width / 2
        favoriteButton.setImage(#imageLiteral(resourceName: "plIconFavorite"), for: .normal)
        favoriteButton.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(20)
            $0.size.equalTo(favoriteButtonSize)
        }
    }
    
    func setupAddNewSightingButton(){
        addSubview(addNewSightingButton)
        addNewSightingButton.backgroundColor = .flowrPink
        addNewSightingButton.titleLabel?.font = .custom(type: .regular, size: 12)
        addNewSightingButton.setTitleColor(.white, for: .normal)
        addNewSightingButton.layer.cornerRadius = 4
        addNewSightingButton.setTitle("Add new sighting", for: .normal)
        addNewSightingButton.addTarget(self, action: #selector(addNewSightingButtonPressed(sender:)), for: .touchUpInside)

        addNewSightingButton.snp.makeConstraints{
            $0.width.equalTo(newSightingButtonWidth)
            $0.height.equalTo(newSightingButtonHeight)
            $0.top.equalTo(self.snp.bottom).offset(-newSightingButtonHeight/2)
            $0.leading.equalToSuperview().offset(20)
        }
        
    }
    
    @objc func addNewSightingButtonPressed(sender: UIButton) {
        delegate?.addNewSightingButtonPressed()
    }
    
    func setupSightingsLabel() {
        addSubview(sightingsLabel)
        setSightingsLabel()
        sightingsLabel.font = .custom(type: .regular, size: 12)
        sightingsLabel.textColor = .white
        sightingsLabel.textAlignment = .center
        sightingsLabel.snp.makeConstraints {
            $0.centerY.equalToSuperview()
            $0.left.equalTo(favoriteButton.snp.right).offset(20)
        }
    }
    
    func setSightingsLabel(){
        sightingsLabel.text = "sightings_count".localized(flower?.sightings ?? 0)
    }
    
    func setupLabelWrapperView() {
        addSubview(sightingsLabelWrapperView)
        bringSubviewToFront(sightingsLabel)
        sightingsLabelWrapperView.backgroundColor = .black
        sightingsLabelWrapperView.alpha = 0.5
        sightingsLabelWrapperView.layer.cornerRadius = favoriteButtonSize.width / 2
        sightingsLabelWrapperView.snp.makeConstraints {
            $0.center.equalTo(sightingsLabel.snp.center)
            $0.height.equalTo(favoriteButtonSize.height)
            $0.width.equalTo(sightingsLabel.snp.width).offset(25)
        }
    }
    
    func setupBackgroundImageView() {
        addSubview(backgroundImageView)
        backgroundImageView.kf.indicatorType = .activity
        setBackgroundImageView()
        setBackgroundImageViewConstraints()
        setupBackgroundImageViewGradient()
    }
    
    func setBackgroundImageView(){
        let imageURL = URL(string: "http:\(self.flower?.profilePicture ?? "")")
        backgroundImageView.kf.setImage(with: imageURL)
    }
    
    func setBackgroundImageViewConstraints(){
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.translatesAutoresizingMaskIntoConstraints = false
        backgroundImageView.snp.makeConstraints {
            $0.edges.equalToSuperview()
        }
    }
    
    func setupBackgroundImageViewGradient(){
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = CGRect(origin: .zero, size: bounds.size)
        gradientLayer.colors = [UIColor.clear.cgColor, UIColor.black.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.opacity = 1
        backgroundImageView.layer.addSublayer(gradientLayer)
    }
    
    func setupNameLabel() {
        addSubview(titleLabel)
        titleLabel.font = .custom(type: .regular, size: 30)
        titleLabel.textColor = .white
        titleLabel.textAlignment = .left
        titleLabel.numberOfLines = 0
        titleLabel.snp.makeConstraints {
            $0.top.equalTo(favoriteButton.snp.bottom).offset(10)
            $0.left.equalTo(20)
            $0.right.equalTo(-20)
        }
    }
    
    func setNameLabel(){
        titleLabel.text = flower?.name
    }
    
    func setupLatinNameLabel() {
        addSubview(subtitleLabel)
        subtitleLabel.font = .custom(type: .light, size: 16)
        subtitleLabel.textAlignment = .left
        subtitleLabel.numberOfLines = 0
        subtitleLabel.alpha = 0.7
        subtitleLabel.textColor = .white
        setLatinNameLabel()
        subtitleLabel.snp.makeConstraints {
            $0.left.equalTo(20)
            $0.right.equalTo(-20)
            $0.centerX.equalToSuperview()
            $0.top.equalTo(titleLabel.snp.bottom).offset(10)
        }
    }
    
    func setLatinNameLabel(){
        subtitleLabel.text = flower?.latinName ?? ""
    }
    
    
}


