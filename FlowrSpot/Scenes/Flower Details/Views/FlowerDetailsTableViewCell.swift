//
//  FlowerDetailsTableViewCell.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit

class FlowerDetailsTableViewCell: UITableViewCell {
    
    private let featuresLabel = UILabel.autolayoutView()
    private let descriptionLabel = UILabel.autolayoutView()
    private var flower:Flower?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        setFeaturesLabelConstraints()
        setDescriptionLabelConstraints()
    }
    
    func setupViews(){
        setupFeaturesLabel()
        setupDescriptionLabel()
    }
    
    func configureCell(wiht flower:Flower){
        self.flower = flower
        layoutSubviews()
        setFeaturesLabel()
        setDescriptionLabel()
        
    }
    
}

extension FlowerDetailsTableViewCell {
    
    func setupFeaturesLabel(){
        addSubview(featuresLabel)
        featuresLabel.font = .custom(type: .semibold, size: 12)
        featuresLabel.textAlignment = .left
        featuresLabel.numberOfLines = 0
        featuresLabel.alpha = 1
        featuresLabel.textColor = .gray
        setFeaturesLabel()
        
    }
    
    func setFeaturesLabelConstraints(){
        featuresLabel.snp.makeConstraints{
            $0.topMargin.leadingMargin.equalTo(15)
            $0.trailingMargin.equalTo(-20)
        }
    }
    
    func setFeaturesLabel(){
        guard let features = flower?.features else {return}
        featuresLabel.text = features.joined(separator: "\n")
    }
    
    func setupDescriptionLabel(){
        addSubview(descriptionLabel)
        descriptionLabel.font = .custom(type: .regular, size: 12)
        descriptionLabel.textAlignment = .left
        descriptionLabel.numberOfLines = 0
        descriptionLabel.alpha = 1
        descriptionLabel.textColor = .gray
        setDescriptionLabel()
    }
    
    func setDescriptionLabelConstraints(){
        descriptionLabel.snp.makeConstraints{
            $0.top.equalTo(featuresLabel.snp.bottom).offset(20)
            $0.leading.equalTo(featuresLabel.snp.leading)
            $0.trailing.equalTo(featuresLabel.snp.trailing)
            $0.bottom.equalTo(self).offset(-20)
        }
    }
    
    func setDescriptionLabel() {
        guard let flowerDescription = flower?.flowerDescription else {return}
        descriptionLabel.text = flowerDescription
        descriptionLabel.sizeToFit()
    }
}
