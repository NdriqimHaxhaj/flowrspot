//
//  FlowerSightingTableViewCell.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import PovioKit
import Kingfisher

class FlowerSightingTableViewCell: UITableViewCell {
    
    private let sightingFlowerImageView = UIImageView.autolayoutView()
    private let flowerImageViewHeight = 280
    private let profileImageView = UIImageView.autolayoutView()
    private let profileImageViewHeight = 40
    private let nameLabel = UILabel.autolayoutView()
    private let nameFontSize:CGFloat = 18
    private let userLabel = UILabel.autolayoutView()
    private let defaultFontSize:CGFloat = 12
    private let descriptionLabel = UILabel.autolayoutView()
    private let dividerView = UIView.autolayoutView()
    private let favoriteButton = UIButton.autolayoutView()
    private let commentsButton = UIButton.autolayoutView()
    private var sighting:Sighting?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setupViews(){
        setupFlowerImage()
        setupProfileImageView()
        setupNameLabel()
        setupUserLabel()
        setupDescriptionLabel()
        setupDividerView()
        setupFavoriteButton()
        setupCommentsButton()
    }
    
    func configureCell(with sighting:Sighting) {
        self.sighting = sighting
        setFlowerImageView()
        setProfileImageView()
        setNameLabel()
        setUserLabel()
        setDescriptionLabel()
        setFavoriteButton()
        setCommentsButton()
    }
    
}

extension FlowerSightingTableViewCell {
    func setupFlowerImage(){
        addSubview(sightingFlowerImageView)
        sightingFlowerImageView.kf.indicatorType = .activity
        sightingFlowerImageView.contentMode = .scaleAspectFill
        sightingFlowerImageView.clipsToBounds = true
        setFlowerImageView()
        sightingFlowerImageView.snp.makeConstraints {
            $0.top.left.right.equalToSuperview()
            $0.height.equalTo(flowerImageViewHeight)
        }
    }
    func setFlowerImageView(){
        let sightingPictureURL = URL(string: "http:\(self.sighting?.picture ?? "")")
        sightingFlowerImageView.kf.setImage(with: sightingPictureURL)
    }
    
    func setupProfileImageView(){
        addSubview(profileImageView)
        profileImageView.kf.indicatorType = .activity
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.clipsToBounds = true
        profileImageView.layer.cornerRadius = CGFloat(profileImageViewHeight / 2)
        setProfileImageView()
        profileImageView.snp.makeConstraints{
            $0.top.equalTo(sightingFlowerImageView.snp.bottom).offset(20)
            $0.left.equalToSuperview().offset(20)
            $0.width.height.equalTo(profileImageViewHeight)
        }
    }
    
    func setProfileImageView(){
        let sightingPictureURL = URL(string: "http:\(self.sighting?.picture ?? "")")
        profileImageView.kf.setImage(with: sightingPictureURL)
    }
    
    func setupNameLabel(){
        addSubview(nameLabel)
        nameLabel.font = .custom(type: .bold, size: nameFontSize)
        nameLabel.textAlignment = .left
        nameLabel.textColor = .darkGray
        nameLabel.numberOfLines = 1
        nameLabel.alpha = 1
        setNameLabel()
        nameLabel.snp.makeConstraints{
            $0.top.equalTo(profileImageView.snp.top)
            $0.left.equalTo(profileImageView.snp.right).offset(20)
            $0.height.equalTo(nameFontSize)
            $0.right.equalToSuperview().offset(-20)
        }
    }
    
    func setNameLabel(){
        guard let name = sighting?.name else {return}
        nameLabel.text = name
    }
    
    func setupUserLabel(){
        addSubview(userLabel)
        userLabel.font = .custom(type: .italic, size: defaultFontSize)
        userLabel.textAlignment = .left
        userLabel.textColor = .gray
        userLabel.numberOfLines = 1
        userLabel.alpha = 1
        userLabel.snp.makeConstraints{
            $0.top.equalTo(nameLabel.snp.bottom)
            $0.left.equalTo(profileImageView.snp.right).offset(20)
            $0.height.equalTo(nameFontSize)
            $0.right.equalToSuperview().offset(-20)
        }
    }
    
    func setUserLabel(){
        guard let user = sighting?.user?.fullName else {return}
        userLabel.text = "by \(user)"
    }
    
    func setupDescriptionLabel(){
        addSubview(descriptionLabel)
        descriptionLabel.font = .custom(type: .regular, size: defaultFontSize)
        descriptionLabel.textAlignment = .left
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = .gray
        descriptionLabel.alpha = 1
        descriptionLabel.snp.makeConstraints {
            $0.top.equalTo(profileImageView.snp.bottom).offset(20)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
        }
    }
    
    func setDescriptionLabel(){
        guard let descriptionText = sighting?.sightingDescription else {return}
        descriptionLabel.text = descriptionText
    }
    
    func setupDividerView(){
        addSubview(dividerView)
        dividerView.backgroundColor = .gray
        dividerView.snp.makeConstraints {
            $0.top.equalTo(descriptionLabel.snp.bottom).offset(20)
            $0.leading.equalToSuperview().offset(20)
            $0.trailing.equalToSuperview().offset(-20)
            $0.height.equalTo(0.5)
        }
    }
    
    func setupFavoriteButton(){
        addSubview(favoriteButton)
        favoriteButton.setImage(#imageLiteral(resourceName: "plIconFavorite"), for: .normal)
        favoriteButton.setTitleColor(.gray, for: .normal)
        favoriteButton.titleLabel?.font =  .custom(type: .regular, size: defaultFontSize)
        favoriteButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        favoriteButton.snp.makeConstraints{
            $0.top.equalTo(dividerView.snp.bottom).offset(20)
            $0.leading.equalTo(dividerView.snp.leading)
            $0.trailing.equalTo(self.snp.centerX)
            $0.bottom.equalTo(self).offset(-20)
        }
        
    }
    
    func setFavoriteButton(){
        guard let favorites = sighting?.likesCount else {return}
        let buttonTitle = favorites == 1 ? "Favorite" : "Favorites"
        favoriteButton.setTitle(buttonTitle, for: .normal)
    }
    
    func setupCommentsButton(){
        addSubview(commentsButton)
        commentsButton.setImage(#imageLiteral(resourceName: "plIconComment"), for: .normal)
        commentsButton.setTitleColor(.gray, for: .normal)
        commentsButton.titleLabel?.font =  .custom(type: .regular, size: defaultFontSize)
        commentsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20)
        commentsButton.snp.makeConstraints{
            $0.top.equalTo(dividerView.snp.bottom).offset(20)
            $0.leading.equalTo(self.snp.centerX)
            $0.trailing.equalTo(dividerView.snp.trailing)
            $0.bottom.equalTo(self).offset(-20)
        }
    }
    
    func setCommentsButton(){
        guard let favorites = sighting?.commentsCount else {return}
        let buttonTitle = favorites == 1 ? "Comment" : "Comments"
        commentsButton.setTitle(buttonTitle, for: .normal)
    }
}
