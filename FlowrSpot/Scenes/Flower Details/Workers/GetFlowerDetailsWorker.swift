//
//  GetFlowerDetailsWorker.swift
//  FlowrSpot
//
//  Created by Ndriqim Haxhaj on 7/31/19.
//  Copyright © 2019 PovioLabs. All rights reserved.
//

import Foundation

enum FlowerDataType {
    case sightings
    case details
}

class GetFlowerDetailsWorker {
    var downloader = FlowersDownloader()
    
    
    func details(flowerId: Int, flowerDataType: FlowerDataType,
                 success: RestClient.SuccessCompletion<FlowerDetails>,
                 failure: RestClient.FailureCompletion) {
        
        downloader.fetchFlowerDetails(flowerId: flowerId,
                                      success: success,
                                      failure: failure)
        
    }
    
    func sightings(flowerId: Int, flowerDataType: FlowerDataType,
                   success: RestClient.SuccessCompletion<FlowerSightings>,
                   failure: RestClient.FailureCompletion) {
        
        downloader.fetchFlowerSightings(flowerId: flowerId,
                                        success: success,
                                        failure: failure)
        
    }
}
